#
SHELL=/bin/bash
HOST=localhost
PREFIX=/tmp
PROJECT=cd

DATE=$(shell date '+%s')

.PHONY: .ht clean conflict

.ht:	
	echo ${DATE} > $@
	git status --porcelain $@
	git commit -m ${DATE} $@
	git push

clean:	
	git checkout .ht

conflict:
	truncate --size=0 .ht

pull-ssh:	
	ssh -v $(HOST) "cd $(PREFIX)/$(PROJECT) && git pull"

